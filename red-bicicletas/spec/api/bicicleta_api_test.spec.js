var mongoose = require("mongoose");
var Bicicleta = require('../../models/bicicleta');

var server = require('../../bin/www');
var request = require('request');


describe('Bicicleta API', () => {

    afterEach(async function () {
        await Bicicleta.deleteMany({});
    });


    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get('http://localhost:3000/api/bicicletas', async function (error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe(' POST BICICLETAS /create', () => {
        it('Status 200', (done) => {

            var headers = { 'content-type': 'application/json' };
            var abici = '{ "code":10,"color":"rojo","modelo":"Urbana","lat": 10.975832,"lng": -74.808815 }';

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: abici
            },
                function (error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body).bicicleta;
                    console.log(bici);
                    expect(bici.color).toBe('rojo');
                    expect(bici.ubicacion[0]).toBe(10.975832);
                    expect(bici.ubicacion[1]).toBe(-74.808815);
                    done();
                });
        });
    });

    describe(' UPDATE BICICLETAS /:id/update', () => {
        it('Status 200', (done) => {

            var aBici = {code: 1, color: 'verde', modelo: 'urbana', ubicacion: [-3.680331, -79.682571]};
            Bicicleta.add(aBici);

            var headers = { 'content-type': 'application/json' };
            var updateBici = '{ "code":1,"color":"Red","modelo":"Urbana", "ubicacion": [-3.680331, -79.682571] }';

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/1/update',
                body: updateBici
            },
                async function (error, response, body) {
                    expect(response.statusCode).toBe(200);
                    const bici_en = await Bicicleta.findByCode(1);
                    expect(bici_en.color).toBe('Red');
                    done();
                });
        });
    });

    describe(' DELETE BICICLETAS /delete', () => {
        it('Status 200', (done) => {

            var aBici = {code: 1, color: 'verde', modelo: 'urbana', ubicacion: [-3.680331, -79.682571]};
            Bicicleta.add(aBici);

            var headers = { 'content-type': 'application/json' };
            var abici = '{ "id":1 }';

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: abici
            },
                async function (error, response, body) {
                    const bicicletas = await Bicicleta.allBicis();
                    expect(response.statusCode).toBe(204);
                    expect(bicicletas.length).toBe(0);
                    done();
                });
        });
    });
});

