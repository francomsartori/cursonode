let mongoose = require('mongoose');
let Schema = mongoose.Schema;
const Reserva = require('./reserva');

let usuarioSchema = new Schema({
    nombre: String
});

usuarioSchema.methods.reservar = async function(bicicleta, desde, hasta){
    const reserva = new Reserva({ usuario: this._id, bicicleta, desde, hasta });
    console.log(reserva);
    await reserva.save();
};

module.exports = mongoose.model('Usuario', usuarioSchema);